#![crate_name = "heapmon"]

use core::fmt::{self, Debug, Formatter};

use std::alloc::{GlobalAlloc, Layout, System};
use std::cell::RefCell;
use std::cmp::{self, Ordering};
use std::collections::HashMap;
use std::fs::{self, File};
use std::sync::Mutex;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering::Relaxed};
use std::{thread, time::{self, UNIX_EPOCH}};

use regex::Regex;

use core::ffi::c_void;

use backtrace;
use once_cell::sync::Lazy;

use log::*;

pub mod util;

thread_local! {
    static ENTERED: RefCell<bool> = RefCell::new(false);
}

const NSKIP: usize = 8; // skip "internal" frames (heapmon itself)
type BacktraceRec = Vec<usize>;

struct DebugBacktraceRec<'a>(pub &'a BacktraceRec);
impl<'a> Debug for DebugBacktraceRec<'a> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.debug_list()
            .entries(self.0.iter().filter(|ip| **ip != 0).map(|ip| {
                let mut dbgstr = "".to_string();
                backtrace::resolve(*ip as *mut c_void, |s| dbgstr = format!("{:?}", s));
                dbgstr
            }))
            .finish()
    }
}

#[derive(Clone, Eq, PartialEq)]
struct AllocRec {
    size: usize,
    seqno: usize,
    bt: BacktraceRec,
}

impl Debug for AllocRec {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.debug_struct("AllocRec")
            .field("size", &self.size)
            .field("seqno", &self.seqno)
            .field("bt", &DebugBacktraceRec(&self.bt))
            .finish()
    }
}

#[derive(Clone, Debug)]
struct SummaryRec {
    count: usize,
    #[allow(unused)]
    arec: AllocRec,
}

#[derive(Copy, Clone, Debug)]
pub enum SummaryOrder {
    FirstSeen,
    MemoryUsed,
}

fn first_seen(a: &SummaryRec, b: &SummaryRec) -> Ordering {
    (a.arec.seqno).cmp(&(b.arec.seqno))
}

fn memory_used(a: &SummaryRec, b: &SummaryRec) -> Ordering {
    let asz = a.count * a.arec.size;
    let bsz = b.count * b.arec.size;
    if asz != bsz {
        asz.cmp(&bsz).reverse()
    } else {
        (a.arec.seqno).cmp(&(b.arec.seqno))
    }
}

impl SummaryRec {
    fn format_output(&self) -> String {
        let mut output = String::new();
        output += &format!("\n{} bytes = {} blocks * {} bytes, min_seqno {}",
                           pretty_print_usize(self.count * self.arec.size),
                           pretty_print_usize(self.count),
                           pretty_print_usize(self.arec.size),
                           self.arec.seqno);
        let mut ndx = 0;
        for ip in self.arec.bt.iter().filter(|ip| **ip != 0) {
            backtrace::resolve(*ip as *mut c_void, |s| {
                output += &format!("\n  {:>2}: {}", ndx, s.name().unwrap());
                if let Some(filename) = s.filename() {
                    output += &format!("\n      {}:{}",
                                       filename.display(), s.lineno().unwrap_or(0));
                }
            });
            ndx += 1;
        }
        output
    }
}

pub struct HeapMon<A> {
    inner: A,
    is_enabled: AtomicBool,
    seqno: AtomicUsize,
    filters: Mutex<Vec<Regex>>,
    outstanding: Lazy<Mutex<HashMap<usize, AllocRec>>>,
    heapsz: AtomicUsize,
    is_peakhold: AtomicBool,
    peak: Lazy<Mutex<(usize, usize)>>, // (seqno, heapsz)
    history: Lazy<Mutex<Vec<AllocRec>>>,
}

impl<A: Sync> HeapMon<A> {
    /// Creates a new `HeapMon` that wraps another allocator.
    pub const fn from_allocator(allocator: A) -> Self {
        Self {
            inner: allocator,
            is_enabled: AtomicBool::new(false),
            seqno: AtomicUsize::new(0),
            filters: Mutex::new(Vec::new()),
            outstanding: Lazy::new(|| Mutex::new(HashMap::new())),
            heapsz: AtomicUsize::new(0),
            is_peakhold: AtomicBool::new(false),
            peak: Lazy::new(|| Mutex::new((0, 0))),
            history: Lazy::new(|| Mutex::new(Vec::new())),
        }
    }

    /// Add a regex filter
    pub fn filter(&self, regexstr: &str) {
        self.filters.lock().unwrap().push(Regex::new(regexstr).expect("regex"));
    }

    /// Enable heap monitoring
    pub fn enable(&self) {
        info!("heapmon::enable at seqno {}, heapsz {}",
              self.seqno.load(Relaxed), self.heapsz.load(Relaxed));
        self.is_enabled.store(true, Relaxed);
    }

    /// Disable heap monitoring
    pub fn disable(&self) -> (usize, usize) {
        self.is_enabled.store(false, Relaxed);
        let peak = self.peak.lock().unwrap();
        let heapsz = self.heapsz.load(Relaxed);
        info!("heapmon::disable at seqno {}, heapsz {}, peakseqno {}, peakheapsz {}",
              self.seqno.load(Relaxed), heapsz, peak.0, peak.1,
        );
        (heapsz, peak.1)
    }

    /// Enable peakhold monitoring
    pub fn peakhold(&self) {
        info!("heapmon::peakhold at seqno {}, heapsz {}",
              self.seqno.load(Relaxed), self.heapsz.load(Relaxed));
        self.is_peakhold.store(true, Relaxed);
        self.is_enabled.store(true, Relaxed);
    }

    /// Resets all state in the heap monitor except the filters
    pub fn reset(&self) {
        info!("heapmon::reset");
        self.is_enabled.store(false, Relaxed);
        self.seqno.store(0, Relaxed);
        self.outstanding.lock().unwrap().clear();
        self.heapsz.store(0, Relaxed);
        self.is_peakhold.store(false, Relaxed);
        *self.peak.lock().unwrap() = (0, 0);
        self.history.lock().unwrap().clear();
    }

    fn aggregate_record(rollup: &mut HashMap<(BacktraceRec, usize), SummaryRec>,
                            arec: &AllocRec) {
        let key = (arec.bt.clone(), arec.size);
        if !rollup.contains_key(&key) {
            rollup.insert(key.clone(), SummaryRec { count: 0, arec: arec.clone() });
        }
        let srec = rollup.get_mut(&key).unwrap();
        srec.count += 1;
        srec.arec.seqno = cmp::min(srec.arec.seqno, arec.seqno);
    }
    
    /// Dump residual heap allocations
    pub fn dump(&self, summary_order: SummaryOrder, thresh: usize, label: &str) {
        info!("heapmon::dump starting");
        let peak = self.peak.lock().unwrap();
        let is_peakhold = self.is_peakhold.load(Relaxed);
        let mut rollup: HashMap<(BacktraceRec, usize), SummaryRec> = HashMap::new();

        // insert all outstanding allocations
        for (_ptr, arec) in self.outstanding.lock().unwrap().iter() {
            if !is_peakhold || arec.seqno <= peak.0 {
                Self::aggregate_record(&mut rollup, arec);
            }
        }

        // If we are peakhold mode, recover historical deallocations
        for arec in self.history.lock().unwrap().iter() {
            if arec.seqno <= peak.0 {
                Self::aggregate_record(&mut rollup, arec);
            }
        }

        let mut summary: Vec<SummaryRec> = rollup.iter().map(|(_k, srec)| srec.clone()).collect();
        summary.sort_by(match summary_order {
            SummaryOrder::FirstSeen => first_seen,
            SummaryOrder::MemoryUsed => memory_used,
        });
        let mut outsz = 0;
        let mut outbuf = String::new();
        'summaries: for srec in summary.iter() {
            let outstr = srec.format_output();
            for regex in self.filters.lock().unwrap().iter() {
                if regex.is_match(&outstr) {
                    continue 'summaries;
                }
            }
            outsz += srec.count * srec.arec.size;
            outbuf.push_str(&outstr);
            outbuf.push_str("\n");
        }
        if outsz > thresh {
            info!("Filtered peak size for {} is {}", label, pretty_print_usize(outsz));
            info!("{}", outbuf);
        }
        info!("heapmon::dump finished");
    }

    fn allocated(&self, layout: Layout, ptr: *mut u8) {
        let sz = layout.size();
        let seqno = self.seqno.fetch_add(1, Relaxed);
        let heapsz = self.heapsz.fetch_add(sz, Relaxed) + sz;
        let mut arec = AllocRec { size: sz, seqno, bt: Vec::with_capacity(32) };
        let mut ndx = 0;
        backtrace::trace(|frame| {
            if ndx >= NSKIP {
                arec.bt.push(frame.ip() as usize);
            }
            ndx += 1;
            true
        });
        assert_eq!(self.outstanding.lock().unwrap().insert(ptr as usize, arec), None);
        let mut peak = self.peak.lock().unwrap();
        if heapsz > peak.1 {
            *peak = (seqno, heapsz);
            self.history.lock().unwrap().clear();
        }
    }

    fn deallocated(&self, layout: Layout, ptr: *mut u8) {
        // It's ok if we don't find it, maybe we weren't enabled when it was allocated
        if let Some(arec) = self.outstanding.lock().unwrap().remove(&(ptr as usize)) {
            let _heapsz = self.heapsz.fetch_sub(layout.size(), Relaxed) - layout.size();
            if self.is_peakhold.load(Relaxed) {
                self.history.lock().unwrap().push(arec);
            }
        }
    }
}

/// Start the control file watcher thread
pub fn watch<A: Sync>(cp: &str, heapmon: &'static HeapMon<A>, summary_order: SummaryOrder) {
        info!("touch the {} file to enable, touch again to disable and dump", cp);
        let mut watching = false; // start off disabled
        let ctlpath = cp.to_string().clone();
        ctlfile_create(&ctlpath);
        let mut last_mtime = ctlfile_mtime(&ctlpath);
        thread::spawn(move || {
            loop {
                thread::sleep(time::Duration::from_millis(1000));
                let mtime = ctlfile_mtime(&ctlpath);
                if mtime > last_mtime {
                    // The control has been touched.
                    last_mtime = mtime;
                    if !watching {
                        heapmon.reset();
                        heapmon.enable();
                    } else {
                        heapmon.disable();
                        heapmon.dump(summary_order, 0, "watch");
                    }
                    watching = !watching;
                }
            }
        });
}

impl HeapMon<System> {
    /// Creates a new `HeapMon` that wraps the system allocator.
    pub const fn system() -> HeapMon<System> {
        Self::from_allocator(System)
    }
}

fn ctlfile_create(ctlpath: &str) {
    File::create(ctlpath).expect("File::create");
}

fn ctlfile_mtime(ctlpath: &str) -> u64 {
    fs::metadata(ctlpath)
    .unwrap()
    .modified()
    .unwrap()
    .duration_since(UNIX_EPOCH)
    .unwrap()
    .as_secs()
}
    

unsafe impl<A: GlobalAlloc + Sync> GlobalAlloc for HeapMon<A> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let ptr = self.inner.alloc(layout);
        if self.is_enabled.load(Relaxed) {
            ENTERED.with(|e| {
                if !*e.borrow() {
                    *e.borrow_mut() = true;
                    self.allocated(layout, ptr);
                    *e.borrow_mut() = false;
                }
            })
        }
        ptr
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        if self.is_enabled.load(Relaxed) {
            ENTERED.with(|e| {
                if !*e.borrow() {
                    *e.borrow_mut() = true;
                    self.deallocated(layout, ptr);
                    *e.borrow_mut() = false;
                }
            })
        }
        self.inner.dealloc(ptr, layout)
    }
}

fn pretty_print_usize(i: usize) -> String {
    let mut s = String::new();
    let i_str = i.to_string();
    let a = i_str.chars().rev().enumerate();
    for (idx, val) in a {
        if idx != 0 && idx % 3 == 0 {
            s.insert(0, '_');
        }
        s.insert(0, val);
    }
    format!("{}", s)
}

use std::alloc::System;

use log::*;

// This example illustrates how to use the peakhold feature of the heap monitor.

use heapmon::{util::setup_logger, HeapMon, SummaryOrder};

#[global_allocator]
pub static HEAPMON: HeapMon<System> = HeapMon::system();

fn fib (n: i64) -> i64 {
    let strval = format!("blah blah blah blah blah blah");
    if n <= 1 {
        n
    } else {
        _ = strval;
        fib(n - 1) + fib(n - 2)
    }
}

fn main() {
    setup_logger().expect("logger setup");

    const N: i64 = 10;
    HEAPMON.peakhold();
    info!("fib({}) = {}", N, fib(N));
    HEAPMON.disable();
    HEAPMON.dump(SummaryOrder::MemoryUsed, 0, "peakhold");
}

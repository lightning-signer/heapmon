use std::alloc::System;
use std::collections::HashSet;
use std::{thread, time};
use log::*;

// This example uses a control-file watcher to enable and disable the heap monitor

use heapmon::{util::setup_logger, HeapMon, SummaryOrder};

#[global_allocator]
pub static HEAPMON: HeapMon<System> = HeapMon::system();

fn main() {
    setup_logger().expect("logger setup");

    let mut uvals: Vec<usize> = Vec::new();
    let mut svals: HashSet<String> = HashSet::new();

    const CONTROL_PATH: &str = "./heapmon.ctl";
    heapmon::watch(CONTROL_PATH, &HEAPMON, SummaryOrder::FirstSeen);

    const NLOOPS: usize = 120;
    for ndx in 0..NLOOPS {
        svals.insert("foobar".to_string());
        svals.insert("blat".to_string());
        info!("{} seconds left", NLOOPS - ndx - 1);
        uvals.push(ndx);
        svals.insert(format!("{}", ndx));
        thread::sleep(time::Duration::from_millis(1000));
    }
    info!("watcher finished");
}

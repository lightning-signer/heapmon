use std::alloc::System;
use std::collections::HashSet;

// This example instantiates a heap monitor and explicitly enables it around
// several allocating operations.

use heapmon::{util::setup_logger, HeapMon, SummaryOrder};

#[global_allocator]
pub static HEAPMON: HeapMon<System> = HeapMon::system();

fn main() {
    setup_logger().expect("logger setup");

    let mut uvals: Vec<usize> = Vec::new();
    let mut svals: HashSet<String> = HashSet::new();
    
    HEAPMON.enable();

    uvals.push(42);
    svals.insert("foobar".to_string());
    svals.insert("blat".to_string());
    
    HEAPMON.disable();
    HEAPMON.dump(SummaryOrder::MemoryUsed, 0, "explicit");
}

# heapmon

A heap monitor debugging utility for Rust programs

### Examples

The explicit example controls the heap monitor explicitly:
```
cargo run --example explicit
```

The `watcher` example uses a control file to control the heap monitor.
Each time the control file's modification time is updated the
enable/disable state of the heap monitor toggles.

In shell #1:
```
cargo run --example watcher
```

In shell #2:
```
touch heapmon.ctl
sleep 30
touch heapmon.ctl
```
